# SmartBin#

A Universal Windows App for the clients to monitor the garbage level in various dustbins and locate the nearest dustbins. Also useful to plan and schedule the garbage pickup.

### Problem Statement ###

A healthy environment is imperative to a healthy and happy community. With the age old system of hiring people to regularly check and empty filled dustbins, the process has been prone to human error and neglect. Additionally, due to different frequency of usage of dustbins in different areas,  routine checks which are based on time crevices is inefficient because a dustbin might get filled early and may need immediate attention or there might not be any need of a routine check for a long period of time. This makes present system resource expensive and ineffectual, as overflowing, stinking dustbins become more of a problem than a solution.


### Solution ###

SmartBin is a network of dustbins which integrates the idea of IoT with Wireless Sensor Networks. Hundreds of dustbins in an area are embedded with low power and low cost smart sensors that are connected to an IoT device, which acts as a central hub/sink for all the bins. The bins transmit their geo-location and status of it’s contents to the central hub which relays this data to a cloud platform. The cloud platform further pushes the data to the client app in which the current location of the bins and their state (filled or not) is displayed in a map. The client app can be downloaded by the cleanliness department and data can be used to plan their routine check. Automation of garbage monitoring would optimize resources, reduce cases of neglect and is easy to adopt. 
It would save time, fuel and manual labor spent in checking all the dustbins periodically. Instead, a schedule can be developed, with this monitoring system, to ensure that only dustbins which are filled beyond a threshold are emptied, at intervals. This will make the periodic cleaning more efficient and reduce the number of trips required. 
It will also enable the admin to detect neglect of any dustbin, and from gathered data, to know if some areas need additional dustbins installed, based on the frequency of usage there.
For residents, from their home, they can find out the closest empty dustbin to approach and the shortest way to get to it, saving their time and effort. 
