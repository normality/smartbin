﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Storage;

namespace SplitViewDemo
{
    public class Item
    {
        public string Id { get; set; }
        public int level { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public DateTime time { get; set; }
        public string name { get; set; }
        public string location { get; set; }
        public int count { get; set; }
    }
}

//        List<Item> bins = new List<Item>();
//            public const string JSONFILENAME = "data.json";

//        public List<Item> buildObjectGraph()
//        {
//            Item item1 = new Item { level = 60, latitude = 27.961350, longitude = 76.403662, Id = "Smartbin_1" };
//            Item item2 = new Item { level = 60, latitude = 27.961350, longitude = 76.401662, Id = "Smartbin_2" };
//            Item item3 = new Item { level = 60, latitude = 27.962536, longitude = 76.401861, Id = "Smartbin_3" };
//            Item item4 = new Item { level = 60, latitude = 27.963070, longitude = 76.403295, Id = "Smartbin_4" };
//            Item item5 = new Item { level = 60, latitude = 27.962392, longitude = 76.402789, Id = "Smartbin_5" };

//            bins.Add(item1);
//            bins.Add(item2);
//            bins.Add(item3);
//            bins.Add(item4);
//            bins.Add(item5);
            
//            return bins;
//            //throw new NotImplementedException();
//        }
//        public async Task writeJsonAsync()
//        {
//            // Notice that the write is ALMOST identical ... except for the serializer.

//            var myBins = buildObjectGraph();

//            var serializer = new DataContractJsonSerializer(typeof(List<Item>));
//            using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(
//                          JSONFILENAME,
//                          CreationCollisionOption.ReplaceExisting))
//            {
//                serializer.WriteObject(stream, myBins);
//            }

//            //resultTextBlock.Text = "Write succeeded";
//        }
//        public async Task  deserializeJsonAsync()
//        {
            
//            var jsonSerializer = new DataContractJsonSerializer(typeof(List<Item>));

//            var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(JSONFILENAME);

//            myBins = (List<Item>)jsonSerializer.ReadObject(myStream);

//            //return myBins;
//        }
//        List<Item> myBins;
//        public List<Item> readJsonAsync()
//        {
//            deserializeJsonAsync();
//            return myBins;
//        }

//    }
//}
