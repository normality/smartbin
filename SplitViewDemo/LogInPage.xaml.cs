﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SplitViewDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LogInPage : Page
    {
        public LogInPage()
        {
            this.InitializeComponent();
        }
        Userdata uData = new Userdata();
        private  void singupbtn_Click(object sender, RoutedEventArgs e)
        {
            //Item item = new Item { level = 60 , time = DateTime.Now , name = "bin_1" };
            //await App.imagineClient.GetTable<Item>().InsertAsync(item);

            //userInfo info = new userInfo();
            //flyout.ShowAt(sender as FrameworkElement);
        }

        private void loginbtn_Click(object sender, RoutedEventArgs e)
        {
            //List<userInfo> infos = uData.readJsonAsync();
            //var myInfo = from info in infos where logintxt.Text == info.name && passwordBox.Text == info.password select info;
            //if (myInfo.FirstOrDefault()!=null)
            //{
            //    var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            //    localSettings.Values["Logged In"] = true;
            //    localSettings.Values["user"] = myInfo.FirstOrDefault().name;
            //    this.Frame.Navigate(typeof(HomePage));
            //}
            //else
            //{
            //    loginMsgtxtblock.Text = "Invalid Credentials!";
            //    logintxt.Text = "";
            //    passwordBox = null;
            //}
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            localSettings.Values["user"] = "Admin: "+logintxt.Text;
            localSettings.Values["Logged In"] = true;
            this.Frame.Navigate(typeof(HomePage));
        }

        private void areabtn_Click(object sender, RoutedEventArgs e)
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            localSettings.Values["Logged In"] = true;
            localSettings.Values["user"] = "Guest Mode";

            this.Frame.Navigate(typeof(HomePage));

        }

        private async void confirmbtn_Click(object sender, RoutedEventArgs e)
        {
            Userdata mydata = new Userdata();
            
            //await mydata.buildInfo();
        }
    }
}
