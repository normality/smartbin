﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SplitViewDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SearchPage : Page
    {
        public SearchPage()
        {
            this.InitializeComponent();
            var typeUser = (string)Windows.Storage.ApplicationData.Current.LocalSettings.Values["user"];
            if (typeUser == "Guest Mode")
            {
                complaintGrid.Visibility = Visibility.Visible;
                
            }
            else
            {
               
                complaintGrid.Visibility = Visibility.Collapsed;
                listGrid.Visibility = Visibility.Visible;
                deserializeJsonAsync();
            }
            //else {
            //    deserializeJsonAsync();//displayList();
            //}
            //else complaintGrid.Visibility = Visibility.Collapsed;
        }

        //private void displayList()
        //{
        //    List<Complaints> co = new List<Complaints>();
        //    Complaints c1 = new Complaints { MsgTitle = "Title1", Message = "This is a new world",Date = DateTime.Now.Date };
        //    Complaints c2 = new Complaints { MsgTitle = "Title1", Message = "This is a new world", Date = DateTime.Now.Date };
        //    Complaints c3 = new Complaints { MsgTitle = "Title1", Message = "This is a new world", Date = DateTime.Now.Date };
        //    Complaints c4 = new Complaints { MsgTitle = "Title1", Message = "This is a new world", Date = DateTime.Now.Date };
        //    co.Add(c1);
        //    co.Add(c2);
        //    co.Add(c3);
        //    co.Add(c4);
        //    //listComplaints.ItemsSource = co;
        //    //throw new NotImplementedException();
        //}

        public const string ComplaintsJSONFILENAME = "complaints.json";

        private async void sendComplaintBtn_Click(object sender, RoutedEventArgs e)
        {
            Complaints comp = new Complaints();
            comp.MsgTitle = titleCompl.Text;
            comp.Message = msgCompl.Text;
            comp.Priority = priorityFlyoutBtn.Content.ToString();
            comp.Date = DateTime.Now;
            try
            {
                await writeJsonAsync(comp);
                var dialog = new MessageDialog("Complaint Registered");
                dialog.Commands.Add(new UICommand("OK"));
                await dialog.ShowAsync();
            }
            catch (Exception ex)
            {

                var dialog = new MessageDialog(ex.ToString());
                dialog.Commands.Add(new UICommand("OK"));
                await dialog.ShowAsync();
            }
            titleCompl.Text = "";
            msgCompl.Text = "";
            
        }
        public async Task writeJsonAsync(Complaints c)
        {
            // Notice that the write is ALMOST identical ... except for the serializer.
            List<Complaints> listw = new List<Complaints>();
            //Windows.Storage.ApplicationData.Current.LocalSettings.Values["write"] = null;
            if (Windows.Storage.ApplicationData.Current.LocalSettings.Values["write"] == null)
            {
                Windows.Storage.ApplicationData.Current.LocalSettings.Values["write"] = 1;
            }
            else
            {
                listw = await deserializeJsonAsync();
                
            }
            listw.Add(c);
            var serializer = new DataContractJsonSerializer(typeof(List<Complaints>));
            using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(
                          ComplaintsJSONFILENAME,
                          CreationCollisionOption.ReplaceExisting))
            {
                serializer.WriteObject(stream, listw);
            }
            //


        }
        public async Task<List<Complaints>> deserializeJsonAsync()
        {
            List<Complaints> myComplaints = new List<Complaints>();

            var jsonSerializer = new DataContractJsonSerializer(typeof(List<Complaints>));

            var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(ComplaintsJSONFILENAME);

            myComplaints = (List<Complaints>)jsonSerializer.ReadObject(myStream);
            listComplaints.ItemsSource = myComplaints;
            return myComplaints;
            //return myBins;
        }

        private void priorityFlyoutBtn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void highFlyout_Click(object sender, RoutedEventArgs e)
        {
            priorityFlyoutBtn.Content = highFlyout.Text;
        }

        private void mediumFlyout_Click(object sender, RoutedEventArgs e)
        {
            priorityFlyoutBtn.Content = mediumFlyout.Text;
        }

        private void lowFlyout_Click(object sender, RoutedEventArgs e)
        {
            priorityFlyoutBtn.Content = lowFlyout.Text;
        }

        private void StackPanel_Holding(object sender, HoldingRoutedEventArgs e)
        {
            //if (e.HoldingState != HoldingState.Started) return;
            FrameworkElement senderElement = sender as FrameworkElement;
            FlyoutBase flyoutBase = FlyoutBase.GetAttachedFlyout(senderElement);
            flyoutBase.ShowAt(senderElement);
        }

        private async void deleteItem_Click(object sender, RoutedEventArgs e)
        {
            List<Complaints> myComplaints = new List<Complaints>();

            var jsonSerializer = new DataContractJsonSerializer(typeof(List<Complaints>));

            var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(ComplaintsJSONFILENAME);

            myComplaints = (List<Complaints>)jsonSerializer.ReadObject(myStream);
            myComplaints.RemoveRange(0, 1);
            listComplaints.ItemsSource = myComplaints;
            //displayList();
        }
    }
}
