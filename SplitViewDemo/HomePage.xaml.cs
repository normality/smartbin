﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Services.Maps;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SplitViewDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class HomePage : Page
    {
        public HomePage()
        {
            this.InitializeComponent();
            
                this.NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Enabled;

            string u = (string)Windows.Storage.ApplicationData.Current.LocalSettings.Values["user"];
            if(u == "Guest Mode")
            {
                AddDevicebtn.Visibility = Visibility.Collapsed;
                fillBinBtn.Visibility = Visibility.Collapsed;
                displayAllBins();
            }
            //OnNavigated();
        }
        Item myItem = new Item();
        public const string JSONFILENAME = "data.json";

        private  void displayAllBins()
        {
            OnNavigated();
            List<Item> items = buildObjectGraph();
            foreach (var item in items)
            {
                BasicGeoposition b = new BasicGeoposition() { Latitude = item.latitude, Longitude = item.longitude, Altitude = item.level };
                Geopoint g = new Geopoint(b);
                MapIcon m = new MapIcon();
                m.Location = g;
                m.NormalizedAnchorPoint = new Point(.5, 1.0);
                m.ZIndex = 0;
                m.Title = b.Altitude + "%";
                MapControl1.MapElements.Add(m);

            }
            //throw new NotImplementedException();
        }


        List<Item> bins = new List<Item>();
        List<BasicGeoposition> bpos = new List<BasicGeoposition>();
        private void OnNavigated()
        {
            //await myItem.writeJsonAsync();

            BasicGeoposition cityPosition = new BasicGeoposition() { Latitude = 27.97819, Longitude = 76.410026 };
            Geopoint cityCenter = new Geopoint(cityPosition);
            MapControl1.Center = cityCenter;
            MapControl1.ZoomLevel = 16;
            MapControl1.LandmarksVisible = true;
            MapControl1.Style = MapStyle.Terrain;
            //upload();
            //await writeJsonAsync();


        }
        public List<Item> buildObjectGraph()
        {
            Item item1 = new Item { level = 49, latitude = 27.979131, longitude = 76.408400, Id = "Smartbin_1" };
            Item item2 = new Item { level = 90, latitude = 27.977042, longitude = 76.408129, Id = "Smartbin_2" };
            Item item3 = new Item { level = 66, latitude = 27.976607, longitude = 76.411456, Id = "Smartbin_3" };
            Item item4 = new Item { level = 45, latitude = 27.979784, longitude = 76.411505, Id = "Smartbin_4" };
            Item item5 = new Item { level = 90, latitude = 27.979218, longitude = 76.410100, Id = "Smartbin_5" };

            bins.Add(item1);
            bins.Add(item2);
            bins.Add(item3);
            bins.Add(item4);
            bins.Add(item5);

            return bins;
            //throw new NotImplementedException();
        }
        public async Task writeJsonAsync()
        {
            // Notice that the write is ALMOST identical ... except for the serializer.

            var myBins = buildObjectGraph();

            var serializer = new DataContractJsonSerializer(typeof(List<Item>));
            using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(
                          JSONFILENAME,
                          CreationCollisionOption.ReplaceExisting))
            {
                serializer.WriteObject(stream, myBins);
            }

            //resultTextBlock.Text = "Write succeeded";
        }
        public async Task<List<Item>> deserializeJsonAsync()
        {

            var jsonSerializer = new DataContractJsonSerializer(typeof(List<Item>));

            var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(JSONFILENAME);

            var myBins = (List<Item>)jsonSerializer.ReadObject(myStream);

            return myBins;
        }

        private void popupbtn_Click(object sender, RoutedEventArgs e)
        {
            if (!StandardPopup.IsOpen) { StandardPopup.IsOpen = true; }
            loginPopup.Text = (string)Windows.Storage.ApplicationData.Current.LocalSettings.Values["user"];

        }

        private void ClosePopupClicked(object sender, RoutedEventArgs e)
        {
            if (StandardPopup.IsOpen) { StandardPopup.IsOpen = false; }

        }

        private void AddDevicebtn_Click(object sender, RoutedEventArgs e)
        {
            OnNavigated();
            devicetxt.Text = "";
            if (!AddDevicePopup.IsOpen) { AddDevicePopup.IsOpen = true; }
            msgBlock.Text = "";
             
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            
            string dId = devicetxt.Text;
            bins = buildObjectGraph();
            try
            {
                var geoPosition = from bin in bins where bin.Id == dId select bin;
                BasicGeoposition b = new BasicGeoposition() { Latitude = geoPosition.First().latitude, Longitude = geoPosition.First().longitude, Altitude = geoPosition.First().level };
                Geopoint g = new Geopoint(b);
                MapIcon m = new MapIcon();
                m.Location = g;
                m.NormalizedAnchorPoint = new Point(.5, 1.0);
                m.ZIndex = 0;
                m.Title = b.Altitude + "%";
                MapControl1.MapElements.Add(m);
                if (AddDevicePopup.IsOpen) { AddDevicePopup.IsOpen = false; }
            }
            catch (Exception)
            {

                msgBlock.Text = "Invalid!";
                devicetxt.Text = "";
            }
            

        }

        private async void fillBinBtn_Click(object sender, RoutedEventArgs e)
        {
            //OnNavigated();
            BasicGeoposition mylocation = new BasicGeoposition() { Latitude = 27.975590, Longitude = 76.410315 };
            BasicGeoposition binlocation = new BasicGeoposition() { Latitude = 27.979218, Longitude = 76.410100 };
            MapIcon m1 = new MapIcon();
            Geopoint g1 = new Geopoint(binlocation);
            m1.Location = g1;
            m1.ZIndex = 0;
            //m1.Title = "90 %";
            m1.NormalizedAnchorPoint = new Point(.5, 1.0);
            
            //m1.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/dustfill.png"));
            MapControl1.MapElements.Add(m1);

            MapIcon m2 = new MapIcon();
            Geopoint g2 = new Geopoint(mylocation);
            m2.Location = g2;
            m2.NormalizedAnchorPoint = new Point(.5, 1.0);
            m2.ZIndex = 0;
            m2.Title = "start";
            m2.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/start.png"));
            MapControl1.MapElements.Add(m2);
            displayAllBins();

            MapRouteFinderResult routeResult =
                  await MapRouteFinder.GetDrivingRouteAsync(
                  new Geopoint(mylocation),
                  new Geopoint(binlocation),
                  MapRouteOptimization.Time,
                  MapRouteRestrictions.None);

            if (routeResult.Status == MapRouteFinderStatus.Success)
            {
                // Use the route to initialize a MapRouteView.
                MapRouteView viewOfRoute = new MapRouteView(routeResult.Route);
                viewOfRoute.RouteColor = Colors.Blue;
                //viewOfRoute.OutlineColor = Colors.Black;
                
                // Add the new MapRouteView to the Routes collection
                // of the MapControl.
                MapControl1.Routes.Add(viewOfRoute);

                // Fit the MapControl to the route.
                await MapControl1.TrySetViewBoundsAsync(
                      routeResult.Route.BoundingBox,
                      null,
                      Windows.UI.Xaml.Controls.Maps.MapAnimationKind.Bow);

            }
            //generate local notification
            ToastTemplateType toastTemplate = ToastTemplateType.ToastText02;
            var toastDescriptor = ToastNotificationManager.GetTemplateContent(toastTemplate);
            var txtNodes = toastDescriptor.GetElementsByTagName("text");
            txtNodes[0].AppendChild(toastDescriptor.CreateTextNode("SmartBin"));
            txtNodes[1].AppendChild(toastDescriptor.CreateTextNode("Bin 1 is about to get filled!"));
            var toast = new ToastNotification(toastDescriptor);
            var toastNotifier = ToastNotificationManager.CreateToastNotifier();
            toastNotifier.Show(toast);
        }

        

        private void MapControl1_MapTapped(MapControl sender, MapInputEventArgs args)
        {
            Geopoint pointToReverseGeocode = new Geopoint(args.Location.Position);
            MapIcon m = new MapIcon();
            m.Location = pointToReverseGeocode;
            m.NormalizedAnchorPoint = new Point(.5, 1.0);
            m.ZIndex = 0;
            m.Title = "start";
            m.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/start.png"));
            MapControl1.MapElements.Add(m);
        }
    }
}
