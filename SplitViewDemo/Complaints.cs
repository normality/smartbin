﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplitViewDemo
{
    public class Complaints
    {
        public string Id { get; set; }
        public string MsgTitle { get; set; }
        public string Message { get; set; }
        public string Priority { get; set; }
        public DateTime Date { get; set; }
    }
}
