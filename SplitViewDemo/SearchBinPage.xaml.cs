﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SplitViewDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SearchBinPage : Page
    {
        public SearchBinPage()
        {
            this.InitializeComponent();
        }
        string choice = "";
        private void Id_Click(object sender, RoutedEventArgs e)
        {

        }

        private void IdFlyout_Click(object sender, RoutedEventArgs e)
        {
            SelectBtn.Content = "Id";
            choice = "Id";
        }

        private void LocationFlyout_Click(object sender, RoutedEventArgs e)
        {
            SelectBtn.Content = "Location";
            choice = "Location";
        }

        private void All_Click(object sender, RoutedEventArgs e)
        {
            ThresholdBtn.Content = "All";
        }

        private void ten_Click(object sender, RoutedEventArgs e)
        {
            ThresholdBtn.Content = "20 %";

        }

        private void forty_Click(object sender, RoutedEventArgs e)
        {
            ThresholdBtn.Content = "40 %";

        }

        private void sixty_Click(object sender, RoutedEventArgs e)
        {
            ThresholdBtn.Content = "60 %";

        }

        private void eighty_Click(object sender, RoutedEventArgs e)
        {
            ThresholdBtn.Content = "80 %";

        }

        private void searchBox_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            if(choice == "Location")
            {
                List<Item> allBins = buildObjectGraph();
                var filteredItems = allBins.Where(p => p.location == sender.Text).ToList();
                binsList.ItemsSource = filteredItems;
                BinsTextBlk.Text = filteredItems.Count() + "";
            }
            else
            {
                List<Item> allBins = buildObjectGraph();
                var filteredItems = allBins.Where(p => p.Id == sender.Text).ToList();
                binsList.ItemsSource = filteredItems;
                BinsTextBlk.Text = filteredItems.Count() + "";
            }
           
        }

        private void searchBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (string.IsNullOrEmpty(sender.Text))
                searchBox.ItemsSource = null;
            if(choice == "Location")
            {
                List<Item> allBins = buildObjectGraph();
                List<String> Suggestions;
                Suggestions = allBins.Where(p => p.location.StartsWith(sender.Text)).Select(p => p.location).Distinct().ToList();
                searchBox.ItemsSource = Suggestions;
            }
            else
            {
                List<Item> allBins = buildObjectGraph();
                List<String> Suggestions;
                Suggestions = allBins.Where(p => p.Id.StartsWith(sender.Text)).Select(p => p.Id).Distinct().ToList();
                searchBox.ItemsSource = Suggestions;
            }
            
        }
        public List<Item> buildObjectGraph()
        {
            List<Item> bins = new List<Item>();
            Item item1 = new Item { level = 49, latitude = 27.979131, longitude = 76.408400, Id = "Smartbin_1", location = "Neemrana" };
            Item item2 = new Item { level = 90, latitude = 27.977042, longitude = 76.408129, Id = "Smartbin_2", location = "Neemrana" };
            Item item3 = new Item { level = 66, latitude = 27.976607, longitude = 76.411456, Id = "Smartbin_3", location = "Neemrana" };
            Item item4 = new Item { level = 45, latitude = 27.979784, longitude = 76.411505, Id = "Smartbin_4", location = "Delhi" };
            Item item5 = new Item { level = 90, latitude = 27.979218, longitude = 76.410100, Id = "Smartbin_5", location = "Neemrana" };
            Item item6 = new Item { level = 90, latitude = 27.979218, longitude = 76.410100, Id = "Smartbin_5", location = "Nagpur" };
            Item item7 = new Item { level = 90, latitude = 27.979218, longitude = 76.410100, Id = "Smartbin_5", location = "Dehradun" };
            Item item8 = new Item { level = 90, latitude = 27.979218, longitude = 76.410100, Id = "Smartbin_5", location = "Delhi" };


            bins.Add(item1);
            bins.Add(item2);
            bins.Add(item3);
            bins.Add(item4);
            bins.Add(item5);
            bins.Add(item6);
            bins.Add(item7);
            bins.Add(item8);

            return bins;
            //throw new NotImplementedException();
        }

        private void Time_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
