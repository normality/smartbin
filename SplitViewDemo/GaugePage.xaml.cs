﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SplitViewDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class GaugePage : Page
    {
        public GaugePage()
        {
            this.InitializeComponent();
            callService();

        }
        //int count = 0;
        private DispatcherTimer timer = new DispatcherTimer();
        private void callService()
        {
            timer.Interval = TimeSpan.FromSeconds(2);
            timer.Interval = new TimeSpan(0, 0, 4);
            timer.Start();

            timer.Tick += timer_event;
            //throw new NotImplementedException();
        }

        private async void timer_event(object sender, object e)
        {
            //myGrauge1.Value += 27;
            //if(myGrauge1.Value >= 90)
            //{
            //    ToastTemplateType toastTemplate = ToastTemplateType.ToastText02;
            //    var toastDescriptor = ToastNotificationManager.GetTemplateContent(toastTemplate);
            //    var txtNodes = toastDescriptor.GetElementsByTagName("text");
            //    txtNodes[0].AppendChild(toastDescriptor.CreateTextNode("SmartBin"));
            //    txtNodes[1].AppendChild(toastDescriptor.CreateTextNode("Bin  is about to get filled!"));
            //    var toast = new ToastNotification(toastDescriptor);
            //    var toastNotifier = ToastNotificationManager.CreateToastNotifier();
            //    toastNotifier.Show(toast);
            //    timer.Stop();
            //}


            var file1 = await ApplicationData.Current.LocalFolder.GetFileAsync("as.txt");
            using (var netStream = await file1.OpenStreamForReadAsync())
            {
                using (StreamReader readers = new StreamReader(netStream))
                {
                    //runtime = reader.ReadLine();
                    string line;
                    //while ((line = readers.ReadLine()) != null)
                    //{
                    //    //string[] values = line.Split(',').Select(sValue => sValue.Trim()).ToArray();

                    //    myGrauge1.Value = int.Parse(line);

                    //}
                    line = readers.ReadLine();
                    //string[] values = line.Split(',').Select(sValue => sValue.Trim()).ToArray();
                    int l = int.Parse(line);
                    if (l >= 5 && l < 40)
                    {
                        int lev = (int)(100 - (l * 2.4));
                        myGrauge1.Value = lev;
                    }
                    else if (l >= 1 && l <= 5)
                    {
                        int lev = 90;
                        ToastTemplateType toastTemplate = ToastTemplateType.ToastText02;
                        var toastDescriptor = ToastNotificationManager.GetTemplateContent(toastTemplate);
                        var txtNodes = toastDescriptor.GetElementsByTagName("text");
                        txtNodes[0].AppendChild(toastDescriptor.CreateTextNode("SmartBin"));
                        txtNodes[1].AppendChild(toastDescriptor.CreateTextNode("Bin  is about to get filled!"));
                        var toast = new ToastNotification(toastDescriptor);
                        var toastNotifier = ToastNotificationManager.CreateToastNotifier();
                        toastNotifier.Show(toast);
                        myGrauge1.Value = lev;
                    }


                    //string text = System.IO.File.ReadAllText(@"C:\Users\RAHUL\Desktop\trs.txt");
                    //myGrauge1.Value = int.Parse(text);
                }
            }
            //await ApplicationData.Current.LocalFolder.CreateFileAsync("trs1.asc", CreationCollisionOption.ReplaceExisting);
        }





            //string text = System.IO.File.ReadAllText(@"C:\Users\RAHUL\Downloads\trs.txt");
            //string[] values = text.Split(',').Select(sValue => sValue.Trim()).ToArray();
            //if(values[0]=="1")
            //{
            //    myGrauge1.Value = int.Parse(values[2]);
            //}
            //var g_names = await App.imagineClient.GetTable<Item>().ToListAsync();
            //var filtered1 = g_names.Where(p => p.name == "1").ToList();
            //myGrauge1.Value = filtered1.OrderByDescending(x=>x.count).FirstOrDefault().level;
            //var filtered2 = g_names.Where(p => p.name == "2").ToList();
            //myGrauge2.Value = filtered2.OrderByDescending(x => x.count).FirstOrDefault().level;
            //if ((myGrauge1.Value >= 90 || myGrauge2.Value>=90)&& count++ !=2 )
            //{

            //    ToastTemplateType toastTemplate = ToastTemplateType.ToastText02;
            //    var toastDescriptor = ToastNotificationManager.GetTemplateContent(toastTemplate);
            //    var txtNodes = toastDescriptor.GetElementsByTagName("text");
            //    txtNodes[0].AppendChild(toastDescriptor.CreateTextNode("SmartBin"));
            //    txtNodes[1].AppendChild(toastDescriptor.CreateTextNode("Bin  is about to get filled!"));
            //    var toast = new ToastNotification(toastDescriptor);
            //    var toastNotifier = ToastNotificationManager.CreateToastNotifier();
            //    toastNotifier.Show(toast);
            //}
            //throw new NotImplementedException();
        

        
    }
}
