﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml.Controls;

namespace SplitViewDemo
{
    public class userInfo
    {
        public string name { get; set; }
        public PasswordBox password { get; set; }
    }
    public class Userdata
    {
        List<userInfo> uinfos = new List<userInfo>();
        public const string JSONFILENAME1 = "userdata1.json";

        public async Task buildInfo()
        {
            var list = new userInfo() { name = "Rahul" };
            uinfos.Add(list);
            var list1 = new userInfo() { name = "Aks"};
            uinfos.Add(list1);
            var serializer = new DataContractJsonSerializer(typeof(List<userInfo>));
            using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(
                          JSONFILENAME1,
                          CreationCollisionOption.ReplaceExisting))
            {
                serializer.WriteObject(stream, uinfos);
            }
        }
        public async Task getJsonAsync()
        {
            var jsonSerializer = new DataContractJsonSerializer(typeof(List<userInfo>));

            var myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(JSONFILENAME1);

            xinfos = (List<userInfo>)jsonSerializer.ReadObject(myStream);
        }
        List<userInfo> xinfos;
        public List<userInfo> readJsonAsync()
        {
            getJsonAsync();
            return xinfos;
        }
    


}
        
}
